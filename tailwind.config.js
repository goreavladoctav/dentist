module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {

    borderWidth: {
      DEFAULT: '1px',
      '0': '0',
      '2': '2px',
      '3': '3px',
      '4': '4px',
      '6': '6px',
      '8': '8px',
    },
    backgroundImage: {
      'hero-pattern': "url('./src/assets/img/prolident-zambet-larg.jpg')",
      'team-image': "url('./src/assets/img/echipa/team-section.jpg')",
      'hover-image': "linear-gradient(90deg, rgba(255,255,255,0.87) 0%, rgba(255,255,255,0.55) 100%",
    },

    backgroundPosition: {
      'center-right' : 'center  right',
      'center': 'center, center'
    },
    borderRadius: {
      'round': '20px 5px 20px 5px',
      'picture': '100px 20px'
    },

    height: {
      '70' : '70vh',
      '40': '40vh'
    },
    fontFamily: {
      'rubik' : ['"Rubik", sans-serif'],

    },

    colors: {
      blue: '#0075b2',
      gray: '#F4F4F4',
      black: '#231f20',
      black2: '#666666',
      black3: '#000000',
      white: '#ffffff',
      green: '#5bc236'
    },
    extend: {
      height: {
        menuheight: '70px',
      }
    },

  },
  plugins: [],
}

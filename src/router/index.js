import { createRouter, createWebHistory } from 'vue-router'
import TeamView from '@/views/TeamView.vue'
import HomeView from '@/views/HomeView.vue'
import DigitalDentistryView from '@/views/DigitalDentistryView.vue'
import ForPatientsView from '@/views/ForPatientsView.vue'
import ContactView from '@/views/ContactView.vue'
import EmergenciesView from '@/views/EmergenciesView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [

    {
      path: '/',
      name: 'home',
      component: HomeView
    },

    {
      path: '/echipa',
      name: 'echipa',
      component: TeamView
    },

    {
      path: '/stomatologie-digitala',
      name: 'stomatologie-digitala',
      component: DigitalDentistryView
    },

    {
      path: '/pentru-pacienti',
      name: 'pentru-pacienti',
      component: ForPatientsView
    },

    {
      path: '/contact',
      name: 'contact',
      component: ContactView
    },
    {
      path: '/urgente',
      name: 'urgente',
      component: EmergenciesView
    },

  ]
})

export default router
